function navShow() {
    var x = document.getElementById("navLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }

// TURTLE GAMES

let myGamePiece;
let  myBackground;
let myMusic;


  function startGame(){
    myGamePiece = new component(30, 30, "./img/turtle16px.png", 10, 120, "image");
    myBackground = new component(656, 270, "./img/mare.jpg", 0, 0, "background");
    myMusic = new sound("./music/music.mp3");
    myMusic.play();
    myGameArea.start();

}

let myGameArea = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.width = 480;
        this.canvas.height = 270;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[8]);
        // document.getElementById("demoTurtles").innerHTML = this.canvas ;
        this.interval = setInterval(updateGameArea, 20); //aggiorna ogni 20 millisecondi

        window.addEventListener('keydown', function (e) {   //crea un array per poter tenere in considerazione l'inserimento di multipli comandi da keyboard 
          myGameArea.keys = (myGameArea.keys || []);
          myGameArea.keys[e.keyCode] = true;
        })
        window.addEventListener('keyup', function (e) {
          myGameArea.keys[e.keyCode] = false;
        })
    },
    clear : function() {
      this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function component(width, height, color, x, y,type) {
  this.type = type;
  if (type == "image" || type == "background") {
    this.image = new Image();
    this.image.src = color;
  }
  this.width = width;
  this.height = height;
  this.speedX = 0;  //gli speed sono i valori che aumento o diminuisco con i tasti per il movimento !!!
  this.speedY = 0;
  this.x = x;
  this.y = y;    
  this.update = function() {
    ctx = myGameArea.context;
    if (type == "image" || type == "background") {
      ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
      if (type == "background") {
        ctx.drawImage(this.image, this.x + this.width, this.y, this.width, this.height);
      }
    } else {
        ctx.fillStyle = color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}
  this.newPos = function() {
    this.x += this.speedX;
    this.y += this.speedY;
    if (this.type == "background") {
      if (this.x == -(this.width)) {
        this.x = 0;
      }
    }
  }
}

function updateGameArea() {
  // myGameArea.clear();  //per ripulire la traccia che lascia 
  // myGamePiece.x += 1; //per farlo muover di x+1 a ogni aggiornamento 
  
  //INSERIMENTO CON KEYBOARD
  // myGamePiece.speedX = 0;   //per inserire con keyboard
  // myGamePiece.speedY = 0;
  // if (myGameArea.keys && myGameArea.keys[37]) {myGamePiece.speedX = -1; }
  // if (myGameArea.keys && myGameArea.keys[39]) {myGamePiece.speedX = 1; }
  // if (myGameArea.keys && myGameArea.keys[38]) {myGamePiece.speedY = -1; }
  // if (myGameArea.keys && myGameArea.keys[40]) {myGamePiece.speedY = 1; }
 
  
  myBackground.speedX = -1;
  myBackground.newPos();
  myBackground.update();
  myGamePiece.newPos(); //per caricare la nuova posizione aggiornata con lo spostamneto

  myGamePiece.update();
}


//SOUND AREA 
function sound(src) {
  this.sound = document.createElement("audio");
  this.sound.src = src;
  this.sound.setAttribute("preload", "auto");
  this.sound.setAttribute("controls", "none");
  this.sound.style.display = "none";
  document.body.appendChild(this.sound);
  this.play = function(){
    this.sound.play();
  }
  this.stop = function(){
    this.sound.pause();
  }
}

//PER FARLO MUOVERE CON I TASTI 

function move(dir) {
//   var canvas=document.getElementsByTagName("canvas");
// var cw=canvas.width;
// var ch=canvas.height;
  myGamePiece.image.src = "./img/sea-turtle.png";
  if (dir == "up") {myGamePiece.speedY = -1; }
  if (dir == "down") {myGamePiece.speedY = 1; }
  if (dir == "left") {myGamePiece.speedX = -1; }
  if (dir == "right") {myGamePiece.speedX = 1; }
}


//PER LA KEYBOARD 
// function moveup() {
//   myGamePiece.speedY -= 1;
// }

// function movedown() {
//   myGamePiece.speedY += 1;
// }

// function moveleft() {
//   myGamePiece.speedX -= 1;
// }

// function moveright() {
//   myGamePiece.speedX += 1;
// }


function clearmove() {   //PER FERMARLO
  myGamePiece.image.src = "./img/turtle16px.png";
  myGamePiece.speedX = 0;
  myGamePiece.speedY = 0;
}



