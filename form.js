function navShow() {
    var x = document.getElementById("navLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }


  let img= document.createElement("img");
  img.src = "img/smilegif.gif"
  let img2 = document.createElement("img");
  img2.src = "img/stop.gif";

function resolveAfter2Seconds() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('resolved');
      }, 1000);
    });
  }

async function validateForm() {
    let x = document.forms["myForm"]["ut"].value;
    let y = document.forms["myForm"]["psw"].value;
    let z = document.forms["myForm"]["testoInserito"].value;
    let w = document.forms["myForm"]["psw2"].value;
    
    
    if (x == "" || y == "" || z == "") {
        
        // alert("DEVI RIEMPIRE I  CAMPI VUOTI");
        $( "#toggle" ).effect( "shake", {direction: "left", times: 2, distance: 101}, 1300 );
        document.getElementById("errore").innerHTML = "ERRORE, I CAMPI SONO VUOTI";
        document.getElementById("emoji").innerHTML ="";
        document.getElementById("emoji").appendChild(img2);

    } else if (y.length < 4 || x.length <4 ){
        
        // alert("Troppo Corto, minimo 4 caratteri");
        $( "#toggle" ).effect( "shake", {direction: "left", times: 2, distance: 101}, 1300 );
        document.getElementById("errore").innerHTML = "ERRORE, i campi sono corti";
        document.getElementById("emoji").innerHTML ="";
        document.getElementById("emoji").appendChild(img2);
        // document.forms["myForm"].action = "/form.html";
        
        
    } else if ( y != w ){
        $( "#toggle" ).effect( "shake", {direction: "left", times: 2, distance: 101}, 1300 );
        document.getElementById("errore").innerHTML = "ERRORE, LE PASSWORD DEVONO ESSERE UGUALI";
        document.getElementById("emoji").innerHTML ="";
        document.getElementById("emoji").appendChild(img2);
    }
    else {

        document.getElementById("successo").innerHTML = "GRAZIE PER AVERCI CONTATTATO,<br> stiamo registrando i tuoi dati ";
        document.getElementById("errore").innerHTML = "";
        document.getElementById("emoji").innerHTML ="";
        document.getElementById("emoji").appendChild(img);
        let btn=document.getElementById("buttonSubmit");
        let btn2=document.getElementById("buttonReset");
        btn.style.visibility ="hidden";
        btn2.style.visibility ="hidden";

        // alert(" i Tuoi dati sono stati Inviati Correttamente");
        
        const result = await resolveAfter2Seconds();
        btn.setAttribute('type', 'submit');

        // const result2 = await resolveAfter2Seconds();
        // console.log("Executed now");

        const result3 = await resolveAfter2Seconds();
        console.log("PAPERINO now");
        document.forms["myForm"].submit();

        // document.forms["myForm"].action = "/confirm.html";
    
        
    }
}