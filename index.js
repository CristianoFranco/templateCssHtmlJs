function navShow() {
    var x = document.getElementById("navLinks"); //navLinks sono di default su d-none . la  funzione è come se fosse un TOGGLE CUSTOM . 
    if (x.style.display === "block") {
      x.style.display = "none";  //non li mostra più
    } else {
      x.style.display = "block"; // displayblock =>	Displays an element as a block element (like <p>). It starts on a new line, and takes up the whole width . in pratica li manda a capo !
    }
  }

  function darkMode() {
    let body = document.body;
    let bordi = document.querySelectorAll('.border2Footer');
    body.classList.toggle("light-mode");
    
    for(let i = 0; i < bordi.length; i++){
        bordi[i].classList.toggle('active');

    }
 }

//Function dinamic home

  window.addEventListener('scroll', reveal2);
  window.addEventListener('scroll', reveal);
  
  
  function reveal(){
    let reveals = document.querySelectorAll('.reveal');

    for(let i = 0; i < reveals.length; i++){

      let windowheight = window.innerHeight; //prende l'altezza della window//
      let revealtop = reveals[i].getBoundingClientRect().top; // Getbounding prende l'oggetto e il suo posizionamento specifico nel viewport, a seconda del punto  in cui si scrolla i valori cambiano . top prende la distanza dal bordo in alto  del viewport. References: -> 
      // https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect
      let revealpoint = 150; //setto un valore dal quale io voglio che inizi la animazione con lo scrolling. 

      if(revealtop < windowheight - revealpoint){ //se window è 800. revealtop scrollando dal basso sarà alto, come arriva a circa 600/700, avendo il revealpoint fisso a 150, inizia l'animazione! 
        reveals[i].classList.add('active');
        console.log(windowheight);
        console.log(revealtop);
        console.log(revealpoint);


      }
      else{
//           reveals[i].classList.remove('active');  se lo inserisci te lo toglie ogni volta, e quando ri sali e riscendi ti ri fa l'animazione
      }
    }
  }
  function reveal2(){
        let reveals2 = document.querySelectorAll('.reveal2');

        for(let i = 0; i < reveals2.length; i++){

          let windowheight2 = window.innerHeight;
          let revealtop2 = reveals2[i].getBoundingClientRect().top;
          let revealpoint2 = 150;

          if(revealtop2 < windowheight2 - revealpoint2){
            reveals2[i].classList.add('active');
          }
          else{
//	           reveals[i].classList.remove('active');  se lo inserisci te lo toglie ogni volta, e quando ri sali e riscendi ti ri fa l'animazione
          }
        }
      }

      //SCROLLTOP
      // * Scroll to top
const SCROLLTOP = document.querySelector('#scrollTop');
function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) { //prende il valore in pixel dello scroll . se superiore a 20 px esegue il resto della funzione. document.body.scrollTop > 20 è per safari. 
        SCROLLTOP.style.display = 'block';
    } else {
        SCROLLTOP.style.display = 'none';
    }
}
window.onscroll = () => { 
    scrollFunction(); 
};
SCROLLTOP.addEventListener('click', () => {  //Riporta  a zero lo scroll al click 
    document.body.scrollTop = 0; // Safari
    document.documentElement.scrollTop = 0;
});